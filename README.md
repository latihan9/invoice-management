# Aplikasi Invoice Management #

Aplikasi ini dipakai untuk mengelola invoice dan menyambungkan dengan berbagai 
metode pembayaran.
Diantara metode pembayaran yang akan di support antara lain:

* Virtual Account Bank
    * Bank BNI
    * Bank CIMB
    * Bank BSI
    
* E-Wallet
    * OVO
    * GoPay
    
* QR Payment
    * QRIS
  
## Tipe tagihan yang tersedia:

* CLOSED : bayar sesuai nominal. kalau tidak sesuai. ditolak
* OPEN : pembayaran berapapun diterima
* INSTALLMENT : pembayaran diterima selama total akumulasi 
  lebih kecil atau sama dengan nilai tagihan 

### Cara setup Database ##

1. Jalankan Mysql di docker

```
docker run -d \
 --name invoice-db \
 -e MYSQL_ROOT_PASSWORD=sqI46WJn1XWENt7nMX6e \
 -e MYSQL_USER=invoice \
 -e MYSQL_PASSWORD=sqI46WJn1XWENt7nMX6e \
 -e MYSQL_DATABASE=invoicedb \
 -p 3307:3306 \
 mysql:5.7
```
