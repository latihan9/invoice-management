package com.codecorn.invoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoceManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoceManagementApplication.class, args);
	}

}
