package com.codecorn.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data @Table(name = "invoice_type")
@SQLDelete(sql = "UPDATE invoice_type SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record='ACTIVE'")
public class InvoiceType extends BaseEntity {

    @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String code;

    @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String name;
}
