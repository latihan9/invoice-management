package com.codecorn.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;

@Data @Entity
@SQLDelete(sql = "UPDATE invoice_type_provider SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record='ACTIVE'")
public class InvoiceTypeProvider extends BaseEntity {

}
